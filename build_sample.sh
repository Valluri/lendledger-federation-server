#! /bin/bash

set -e

createdb federation_sample

psql federation_sample -e <<-EOS
  CREATE TABLE people (id character varying, name character varying, domain character varying);
  INSERT INTO people (id, name, domain) VALUES 
    ('GBJ3TPXVSDKXJTAHVV46B5MPYYSHT6WHMCWKZ4RM7OOJXEUDCDTGBQBD', 'issuer', 'cateina.com'),
    ('GBOJBGTDXKZTASKFBU5367PA2UCKDOL5GIIGPSWXT6FHCHXHZJG2UJOL', 'giri', 'cateina.com');
EOS
